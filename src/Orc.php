<?php
namespace App;

use App\Humain;

class Orc {
    private int $id;
    private int $vie;
    private int $mana;
    private int $endurance;

    public function __construct(int $vie, int $mana, int $endurance)
    {
        $this->vie = $vie;
        $this->mana = $mana;
        $this->endurance = $endurance;
    }

    


	/**
	 * @return int
	 */
	public function getVie(): int {
		return $this->vie;
	}
	
	/**
	 * @param int $vie 
	 * @return self
	 */
	public function setVie(int $vie): self {
		$this->vie = $vie;
		return $this;
	}

    /**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getMana(): int {
		return $this->mana;
	}
	
	/**
	 * @param int $mana 
	 * @return self
	 */
	public function setMana(int $mana): self {
		$this->mana = $mana;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getEndurance(): int {
		return $this->endurance;
	}
	
	/**
	 * @param int $endurance 
	 * @return self
	 */
	public function setEndurance(int $endurance): self {
		$this->endurance = $endurance;
		return $this;
	}

    public function machette_judo(){
        $this->vie += 10;
        $this->endurance -= 20;
        
        if($this->endurance < 10){
            echo "t'es faible ".'<br>';
        }
        if($this->endurance <= 0){
            echo "Game over".'<br>';
        }
        echo 'Orc endurance = '.$this->endurance.'<br>';
        
    }
}